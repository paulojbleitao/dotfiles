# set default editor
export EDITOR=vim
# set wallpaper
feh --bg-center wallpapers/msmarvelep03.jpg
# double monitor setup (on laptop)
# xrandr --output HDMI-0 --auto --output eDP-1-0 --auto --right-of HDMI-0
# set keyboard layout
setxkbmap -layout us -variant intl
# fix screen tearing
nvidia-settings --assign CurrentMetaMode="DPY-6: nvidia-auto-select +0+0 {ForceCompositionPipeline=On}, DPY-1: nvidia-auto-select +1920+0 {ForceCompositionPipeline=On}"

# startup programs
google-chrome-stable --enable-features=WebUIDarkMode --force-dark-mode &
discord &
noisetorch -i
