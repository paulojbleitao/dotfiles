set updatetime=100

set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
set number
set hlsearch
syntax on

" gitgutter config
highlight! link SignColumn LineNr
highlight GitGutterAdd    guifg=#acc267 ctermfg=2
highlight GitGutterChange guifg=#ddb26f ctermfg=3
highlight GitGutterDelete guifg=#fb9fb1 ctermfg=1

" remove trailing whitespaces on save
autocmd BufWritePre * :%s/\s\+$//e

" youcompleteme config
let g:ycm_autoclose_preview_window_after_completion = 1
