## Dotfiles

My dotfile collection. :slightly_smiling_face:
The i3 configuration is heavily inspired by [i3-starterpack](https://github.com/addy-dclxvi/i3-starterpack). I recommend checking it out!

![Preview of how my desktop looks like.](preview.png)
