#!/bin/awk
BEGIN {
    FS=" |\("
    q="'"
    row=""
}
{
    if ($1 == "declare") {
        printf "declare -A hiragana=("
        for (i=4; i<=NF; i++) row=row $i " ";
        print substr(row, 1, length(row)-2), "[" q kanji q "]=" q hiragana q ")";
    }
    else {
        print $0
    }
}
