#!/bin/awk -f
BEGIN { 
    FS=" |=";
    q="'";
    row="";
}
{
    if ($1 == "kanji") {
        printf "kanji="
        # remove first column
        for (i=2; i<=NF; i++) row=row $i " ";
        # `substr` removes the last two characters (a whitespace and a ")")
        print substr(row, 1, length(row)-2), q kanji q ")";
    }
    else {
        print $0;
    }
}
