#!/bin/bash
# automatically adds a kanji to the ps1 kanji array :)
kanji=$1

read -p "Adding kanji $kanji, confirm? (y/n) " -n 1
echo

if [[ $REPLY =~ ^[Yy]$ ]]
then
    tempfile=$(mktemp)
    awk -v kanji=$kanji -f add_kanji.awk ps1.sh > $tempfile
    cat $tempfile > ps1.sh
    rm -f $tempfile
fi


read -p "Add the hiragana reading for this kanji? (y/n) " -n 1
echo

if [[ $REPLY =~ ^[Yy]$ ]]
then
    read -p "Enter the hiragana: " hiragana
    tempfile=$(mktemp)
    awk -v kanji=$kanji -v hiragana=$hiragana -f add_hiragana.awk ps1.sh > $tempfile
    cat $tempfile > ps1.sh
    rm -f $tempfile
fi
