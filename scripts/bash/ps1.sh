#!/bin/bash
# customizes bash's prompt!
# this file has to be sourced so it imports variables and functions

color_prompt=$1
TERM=$2

kanji=('時' '年' '方' '者' '前' '事' '豚')
declare -A hiragana=(['豚']='ぶた')
pick_kanji() {
    kanji_len=${#kanji[@]}
    selected=${kanji[$RANDOM % $kanji_len]}
    printf "$selected"

    if ! [ -z ${hiragana[$selected]} ]; then
        printf " (${hiragana[$selected]})"
    fi
}

if [ "$color_prompt" = yes ]; then
    # default prompt:
    # PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    PS1="\[\033[01;32m\]\$(pick_kanji '%s') \[\033[01;34m\]\w\[\033[00;33m\]\$(__git_ps1 ' (%s)')\[\033[37m\]\$ \[\033[0m"
else
    PS1='λ \w$(__git_ps1 " (%s)")\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac
