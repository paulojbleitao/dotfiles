#!/bin/awk
BEGIN {
    FS=" |=";
    row="";
}
{
    if ($1 == "kanji") {
        printf "kanji=";
        for (i=2; i<=NF; i++) {
            if (remove != i)
                row=row $i " ";
        }
        if (remove == 2) printf "(";
        if (remove == NF) print substr(row, 1, length(row)-1) ")";
        else printf row;
    } else {
        print $0;
    }
}
