#!/bin/bash
# automatically removes a kanji from the array :D
source ps1.sh

echo "Which kanji do you want to remove?"
echo ${kanji[@]}
read REMOVE

if [[ $REMOVE -gt 0 && $REMOVE -le ${#kanji[@]} ]]
then
    read -p "Removing ${kanji[$((REMOVE-1))]}, confirm? (y/n) " -n 1
    echo
    if [[ $REPLY =~ ^[Nn]$ ]]; then exit; fi

    REMOVE=$((REMOVE+1))
    tempfile=$(mktemp)
    awk -v remove=$REMOVE -f remove_kanji.awk ps1.sh > $tempfile
    cat $tempfile > ps1.sh
    rm -f $tempfile
    echo "All done!"
else
    echo "Out of range!"
fi
